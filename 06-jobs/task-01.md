# Task 1

## Jobs

- create a job using the following configuration :

1. Pod name: hello-job
2. image : busybox
3. container arguments : echo Hello from busybox; sleep 30;
4. make kubernetes terminates the job if it takes more than three minute to execute
5. make it run 3 times one after another

```sh
$ kubectl create job hello-job --image busybox -o yaml --dry-run -- /bin/sh -c 'while true; do echo Hello from busybox; sleep 30; done' > job.yaml
```

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: hello-job
spec:
  activeDeadlineSeconds: 180 # add this line
  completions: 3 # add this line too
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
        - command:
            - /bin/sh
            - -c
            - echo Hello from busybox; sleep 30;
          image: busybox
          name: hello-job
          resources: {}
      restartPolicy: Never
status: {}
```

- create the pod :

```sh
$ k create -f job.yaml
```

After some minutes :

- get the job :

```sh
$ k get job
```

```log
NAME        COMPLETIONS   DURATION   AGE
hello-job   3/3           2m29s      2m56s
```

- check the pods :

```sh
$ k get pod
```

```log
NAME              READY   STATUS      RESTARTS   AGE
hello-job-8gkrz   0/1     Completed   0          3m16s
hello-job-bsrxb   0/1     Completed   0          2m28s
hello-job-nqk6n   0/1     Completed   0          95s
```

Get the job logs :

```sh
$ kubectl logs job/hello-job
```

```log
Found 3 pods, using pod/hello-job-8gkrz
Hello from busybox
```

Check the job status :

```sh
$ kubectl describe job/hello-job
```

```log
...
Duration:                 2m29s
Active Deadline Seconds:  180s
Pods Statuses:            0 Running / 3 Succeeded / 0 Failed
...
Events:
  Type    Reason            Age    From            Message
  ----    ------            ----   ----            -------
  Normal  SuccessfulCreate  5m25s  job-controller  Created pod: hello-job-8gkrz
  Normal  SuccessfulCreate  4m37s  job-controller  Created pod: hello-job-bsrxb
  Normal  SuccessfulCreate  3m44s  job-controller  Created pod: hello-job-nqk6n
  Normal  Completed         2m56s  job-controller  Job completed
```

- clean workspace

```sh
  $ kubectl delete -f job.yaml
```

## CronJob

- create a cronjob using the following configuration :

1. Pod name: hello-job
2. image : busybox
3. container arguments : echo Hello from busybox; sleep 30;
4. make kubernetes terminates the job if it fails more than two times
5. make it run 3 times in parallel
6. run it every 1 minute

```sh
$ kubectl create cronjob hello-job --image busybox --schedule="*/1 * * * *" -o yaml --dry-run -- /bin/sh -c 'while true; do echo Hello from busybox; sleep 30; done' > cronjob.yaml
```

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  creationTimestamp: null
  name: hello-job
spec:
  jobTemplate:
    metadata:
      creationTimestamp: null
      name: hello-job
    spec:
      backoffLimit: 2 # add this line
      parallelism: 3 # add this line
      template:
        metadata:
          creationTimestamp: null
        spec:
          containers:
            - command:
                - /bin/sh
                - -c
                - while true; do echo Hello from busybox; sleep 30; done
              image: busybox
              name: hello-job
              resources: {}
          restartPolicy: OnFailure
  schedule: "*/1 * * * *"
status: {}
```

- create the pod :

```sh
$ k create -f cronjob.yaml
```

After some minutes :

- get the cronjob :

```sh
$ k get cronjob
```

```log
NAME        SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
hello-job   */1 * * * *   False     4        11s             3m29s

```

- get the job :

```sh
$ k get job
```

```log
NAME                   COMPLETIONS   DURATION   AGE
hello-job-1589569320   0/1 of 3      2m25s      2m25s
hello-job-1589569380   0/1 of 3      89s        89s
hello-job-1589569440   0/1 of 3      24s        24s

```

- check the pods :

```sh
$ k get pod
```

```log
NAME                         READY   STATUS    RESTARTS   AGE
hello-job-1589569320-hh5fk   1/1     Running   0          3m31s
hello-job-1589569320-sh8tn   1/1     Running   0          3m31s
hello-job-1589569320-vv5pt   1/1     Running   0          3m31s
hello-job-1589569380-4b75x   1/1     Running   0          2m35s
hello-job-1589569380-t9pn9   1/1     Running   0          2m35s
hello-job-1589569380-wckqz   1/1     Running   0          2m35s
hello-job-1589569440-b8pwd   1/1     Running   0          90s
hello-job-1589569440-h82bp   1/1     Running   0          90s
hello-job-1589569440-rs965   1/1     Running   0          90s
hello-job-1589569500-bgf8c   1/1     Running   0          38s
hello-job-1589569500-dzggm   1/1     Running   0          38s
hello-job-1589569500-t6lz4   1/1     Running   0          38s
```

- clean workspace

```sh
  $ kubectl delete -f cronjob.yaml
```
