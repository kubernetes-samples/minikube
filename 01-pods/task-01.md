# Task 1

Create a busybox pod with only 1 container using this configuration :

1. Pod name: busybox
2. Container name : busybox
3. image : busybox
4. environment variables :
   1. USERNAME: root
   2. PASSWORD: toor
5. serviceAccount: busy-account (you have to create it)
6. command to run : echo Hello kubernetes !!!; sleep 1d
7. resources :
   1. request : 1cpu , 500Mi of memory
   2. limit : 2cpu, 1Gi of memory
8. labels:
   1. run: busybox
   2. app: frontend
9. container port : 8090

Well, you can create this pod without creating a yaml. You can configure it using the command line :

first of all we have to create the service account :

    $ kubectl create sa busy-account

To get the short form of a resource, use this technique :

    $ kubectl api-resources | grep -i serviceaccount

```log
serviceaccounts                   sa                                          true         ServiceAccount
```

So instead of writing serviceaccount you can use `sa`.

Now we can create the pod :

    $ kubectl run busybox --image busybox --env USERNAME=root --env PASSWORD=toor --serviceaccount busy-account --requests=cpu=1,memory=500Mi --limits cpu=2,memory=1Gi --labels app=frontend --port=8090 -o yaml --dry-run --command -- /bin/sh -c 'echo Hello kubernetes !!!; sleep 1d' > pod.yaml

As we've used the run command it'll add the `run=busybox` label automatically.

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    app: frontend
  name: busybox
spec:
  containers:
    - command:
        - /bin/sh
        - -c
        - echo Hello kubernetes !!!; sleep 1d
      env:
        - name: USERNAME
          value: root
        - name: PASSWORD
          value: toor
      image: busybox
      name: busybox
      ports:
        - containerPort: 8090
      resources:
        limits:
          cpu: "2"
          memory: 1Gi
        requests:
          cpu: "1"
          memory: 500Mi
  dnsPolicy: ClusterFirst
  restartPolicy: Always
  serviceAccountName: busy-account
status: {}
```

Now you can create the pod using this command :

    $ kubectl create -f pod.yaml
