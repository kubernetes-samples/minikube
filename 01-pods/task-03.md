# Task 3

Create a YAML file for an nginx pod that runs with the root user and that has this capability `SYS_TIME`.

    $ kubectl run nginx --image nginx --dry-run=client -o yaml > pod.yaml

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx
spec:
  containers:
    - image: nginx
      name: nginx
      resources: {}
      # add this bloc
      securityContext:
        runAsUser: 0
        capabilities:
          add: ["SYS_TIME"]
      # end
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

The root has the user id 0.
