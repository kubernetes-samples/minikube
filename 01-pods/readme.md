# Pods

In this section, we will focus on the Pods objects.

## task-01

In this task we will create a simple pod and configure it using the command line without creating any yaml file.

## task-02

In this task we will create a pod contains two containers. After creating it, we'll check the logs of each container.
