# Task 2

Create a busybox pod with two containers using this configuration :

1. Pod name: busybox

First container :

1. name : busybox1
2. image : busybox
3. container port : 8090
4. container arguments : while true; do echo Hello from busybox1; sleep 30; done

Second container :

1. name : busybox2
2. image : busybox
3. container port : 8090
4. container arguments : while true; do echo Hello from busybox2; sleep 30; done

Print the logs from container 1 to the standard output. Do the same with the second container.

To create a pod with a single container :

    $ kubectl run busybox --image busybox --port=8090 -o yaml --dry-run -- /bin/sh -c 'while true; do echo Hello from busybox1; sleep 30; done' > pod.yaml

And this what we have to change :

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: busybox
  name: busybox
spec:
  containers:
    - args:
        - /bin/sh
        - -c
        - while true; do echo Hello from busybox1; sleep 30; done
      image: busybox
      name: busybox1 # change this line
      ports:
        - containerPort: 8090
      resources: {}
    # Add this bloc
    - args:
        - /bin/sh
        - -c
        - while true; do echo Hello from busybox2; sleep 30; done
      image: busybox
      name: busybox2
      ports:
        - containerPort: 8090
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

To copy/paste this file you can use this technique if you have a bash terminal :

1. copy the file
2. run this command from your terminal :
   \$ cat > pod.yaml
3. paste the content of the file
4. click Enter
5. click Ctrl+C

Now you can create the pod using this command :

    $ kubectl create -f pod.yaml

Check the pods status :

    $ kubectl get pods

```log
NAME      READY   STATUS    RESTARTS   AGE
busybox   2/2     Running   0          33s
```

As we can see the two containers are ready. Check logs of the first one :

    $ kubectl logs busybox -c busybox1

```log
Hello from busybox1
Hello from busybox1
Hello from busybox1
```

Check the second container :

    $ kubectl logs busybox -c busybox2

```log
Hello from busybox2
Hello from busybox2
Hello from busybox2
Hello from busybox2
```

Clean workspace :

    $ kubectl delete -f pod.yaml
