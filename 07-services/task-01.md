# Task 1

- create an nginx pod and expose it on port 80 :

```sh
$ kubectl run nginx --image nginx --port 80 --expose
```

```log
service/nginx created
pod/nginx created
```

the expose will create a cluster ip service to expose the service.

- check the service :

```sh
$ k get service/nginx -o wide
NAME    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE     SELECTOR
nginx   ClusterIP   10.111.28.219   <none>        80/TCP    2m27s   run=nginx

$ k get pods -o wide
NAME    READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
nginx   1/1     Running   0          2m41s   172.18.0.4   minikube   <none>           <none>
```

- delete the nginx service :

```sh
$ k delete svc nginx
```

- expose the nginx pod using a node port service named nginx-svc on port 8080 :

```sh
$ k expose pod nginx --name nginx-svc --target-port 8080 --port 80 --type NodePort
```

- get the service details :

```sh
$ kubectl describe svc nginx-svc
```

```log
Name:                     nginx-svc
Namespace:                default
Labels:                   run=nginx
Annotations:              <none>
Selector:                 run=nginx
Type:                     NodePort
IP:                       10.110.63.237
Port:                     <unset>  80/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  30141/TCP
Endpoints:                172.18.0.4:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

```sh
$ kubectl get svc nginx-svc -o yaml
```

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2020-05-15T22:30:09Z"
  labels:
    run: nginx
  # ...
  name: nginx-svc
  namespace: default
  resourceVersion: "933754"
  selfLink: /api/v1/namespaces/default/services/nginx-svc
  uid: 4475c724-29f0-47c0-a1d4-a6eb2af78019
spec:
  clusterIP: 10.110.63.237
  externalTrafficPolicy: Cluster
  ports:
    - nodePort: 30141
      port: 80
      protocol: TCP
      targetPort: 8080
  selector:
    run: nginx
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```

- get the service node port :

```sh
$ kubectl get svc nginx-svc -o wide
```

```log
NAME        TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE    SELECTOR
nginx-svc   NodePort   10.110.63.237   <none>        80:30141/TCP   103s   run=nginx
```

- clean workspace

```sh
  $ kubectl delete svc/nginx-svc pod/nginx
```
