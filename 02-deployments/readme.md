# Deployment

In this section, we will focus on the Deployments objects.

## task-01

In this task we will create a deployment and configure it. We will show how to scale deployments up and down, pause them and update them.
