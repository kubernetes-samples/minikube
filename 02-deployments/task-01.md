# Task 1

- Check if there is any deployment in all namespaces :

```sh
  $ kubectl get deploy --all-namespaces
```

- Create a deployment using this configuration :

1. Deployment name : static-site
2. Pod name : static-site-pod
3. image : mouhamedali/static-site-nodejs:v1.0.0
4. replicas: 2
5. container port : 8080

```sh
    $ kubectl create deploy static-site --image mouhamedali/static-site-nodejs:v1.0.0 -o yaml --dry-run > deploy.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: static-site
  name: static-site
spec:
  replicas: 2 # change this line
  selector:
    matchLabels:
      app: static-site
  strategy: {}
  template:
    metadata:
      name: static-site-pod # add this line
      creationTimestamp: null
      labels:
        app: static-site
    spec:
      containers:
        - image: mouhamedali/static-site-nodejs:v1.0.0
          name: static-site-nodejs
          ports:
            - containerPort: 8000 # add this line
          resources: {}
status: {}
```

Now you can create the deployment using this command :

    $ kubectl create -f deploy.yaml

```log
$ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
static-site-7ff8dcfcdd-fcrp8   1/1     Running   0          2m59s
static-site-7ff8dcfcdd-kgvj2   1/1     Running   0          2m59s

$ k get deploy
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
static-site   2/2     2            2           3m5s
```

The `k` is an alias of `kubectl`. you add it to your bashrc file by adding this line `alias k=kubectl`.

- scale down the replicas to only one replica

```sh
$ kubectl scale deploy static-site --replicas 1
```

- get the pod ip address

```sh
$ kubectl get pods -o wide
```

```log
NAME                           READY   STATUS    RESTARTS   AGE   IP           NODE       NOMINATED NODE   READINESS GATES
static-site-7ff8dcfcdd-kgvj2   1/1     Running   0          11m   172.18.0.7   minikube   <none>           <none>
```

- create an another pod and hit the /version path of the pod using an http request to get the current version :

```sh
$ kubectl run busybox --image busybox --rm -it -- sh
/ # wget -O- 172.18.0.7:8000/version
Connecting to 172.18.0.7:8000 (172.18.0.7:8000)
writing to stdout
{
    color: blue,
    version: static-site-nodejs:v1.0.0
-                    100% |************************************************************************************************************************************************************|    61  0:00:00 ETA
written to stdout
/ # exit
```

As we can see the version is v1.0.0 and the color is blue.
the --rm is used to delete the pod automatically after exit.

- update the deployment image to use the image mouhamedali/static-site-nodejs:v2.0.0

```sh
$ kubectl set image deploy static-site static-site-nodejs=static-site-nodejs:v2.0.0 --record
```

static-site-nodejs is the container name to update.

-- record is used to store the command used to update the deployment.

- check that the deployment has been successfully updated (without problems)

```sh
$ k rollout status deploy static-site
Waiting for deployment "static-site" rollout to finish: 1 old replicas are pending termination...

$ k get pods
NAME                           READY   STATUS         RESTARTS   AGE
static-site-7ff8dcfcdd-kgvj2   1/1     Running        0          26m
static-site-877c4645b-xxfmf    0/1     ErrImagePull   0          2m6s
```

As we can see the deployment is failed as the introduced image does no exist. I forget to add my docker hub id. Now we have to rollback the deployment and reintroduce the right docker image.

```sh
$ kubectl rollout undo deployment static-site
```

```log
$ k rollout status deploy static-site
deployment "static-site" successfully rolled out

$ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
static-site-7ff8dcfcdd-kgvj2   1/1     Running   0          32m
```

- Installation of the second version :

```sh
$ kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v2.0.0 --record
```

```log
$ k rollout status deploy static-site
deployment "static-site" successfully rolled out

$ k get pods
NAME                          READY   STATUS    RESTARTS   AGE
static-site-78c9f779c-nd8l9   1/1     Running   0          81s

$ k describe pod static-site-78c9f779c-nd8l9 | grep -i Image
    Image:          mouhamedali/static-site-nodejs:v2.0.0
```

- check the deployment history :

```log
$ k rollout history deploy static-site
deployment.apps/static-site
REVISION  CHANGE-CAUSE
2         kubectl set image deploy static-site static-site-nodejs=static-site-nodejs:v2.0.0 --record=true
3         <none>
4         kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v2.0.0 --record=true
```

- display the details of the 4 revision :

```log
$ k rollout history deploy static-site --revision=4
deployment.apps/static-site with revision #4
Pod Template:
  Labels:	app=static-site
	pod-template-hash=78c9f779c
  Annotations:	kubernetes.io/change-cause: kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v2.0.0 --record=true
  Containers:
   static-site-nodejs:
    Image:	mouhamedali/static-site-nodejs:v2.0.0
    Port:	8000/TCP
    Host Port:	0/TCP
    Environment:	<none>
    Mounts:	<none>
  Volumes:	<none>
```

- Pause the deployment and update it to a random image like `mouhamedali/static-site-nodejs:v9.9.9`. Check the deployment status :

```log
$ k rollout pause deployment static-site
deployment.apps/static-site paused

$ kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v9.9.9 --record
deployment.apps/static-site image updated

$ k rollout status deployment static-site
Waiting for deployment "static-site" rollout to finish: 0 out of 1 new replicas have been updated...

$ k get pods
NAME                          READY   STATUS    RESTARTS   AGE
static-site-78c9f779c-nd8l9   1/1     Running   0          15m

```

- resume the deployment and recheck the deployment :

```log
$ k rollout resume deployment static-site
deployment.apps/static-site resumed

$ k get pods
NAME                          READY   STATUS             RESTARTS   AGE
static-site-78c9f779c-nd8l9   1/1     Running            0          32m
static-site-7b7ddd796-2nzkr   0/1     ImagePullBackOff   0          14m
```

- check the history and go back to the third revision :

```log
$ k rollout history deployment static-site
deployment.apps/static-site
REVISION  CHANGE-CAUSE
2         kubectl set image deploy static-site static-site-nodejs=static-site-nodejs:v2.0.0 --record=true
3         <none>
6         kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v2.0.0 --record=true
7         kubectl set image deploy static-site static-site-nodejs=mouhamedali/static-site-nodejs:v9.9.9 --record=true

$ k rollout history deployment static-site --revision 3
deployment.apps/static-site with revision #3
Pod Template:
  Labels:	app=static-site
	pod-template-hash=7ff8dcfcdd
  Containers:
   static-site-nodejs:
    Image:	mouhamedali/static-site-nodejs:v1.0.0
    Port:	8000/TCP
    Host Port:	0/TCP
    Environment:	<none>
    Mounts:	<none>
  Volumes:	<none>

$ k rollout undo deployment static-site --to-revision 3
deployment.apps/static-site rolled back

```

- check the deployment :

```log
$ k describe deployments static-site | grep Image
    Image:        mouhamedali/static-site-nodejs:v1.0.0

$ k get pods
NAME                           READY   STATUS    RESTARTS   AGE
static-site-7ff8dcfcdd-gnbfw   1/1     Running   0          82s

$ k get deployments
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
static-site   1/1     1            1           74m

```

- delete the deployment :

```sh
$ kubectl delete -f deploy.yaml
```
