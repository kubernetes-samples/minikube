# Task 1

- create a secret named cuser with value username=root :

```sh
  $ kubectl create secret generic cuser --from-literal=username=root

  $ kubectl describe secret cuser
```

```log
Name:         cuser
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
username:  4 bytes
```

- can you display the value of the username from the secret :

```sh
$ kubectl get secret cuser -o yaml
```

```log
apiVersion: v1
data:
  username: cm9vdA==
kind: Secret
metadata:
  creationTimestamp: "2020-05-15T12:16:49Z"
  managedFields:
  - apiVersion: v1
    fieldsType: FieldsV1
    fieldsV1:
      f:data:
        .: {}
        f:username: {}
      f:type: {}
    manager: kubectl
    operation: Update
    time: "2020-05-15T12:16:49Z"
  name: cuser
  namespace: default
  resourceVersion: "860097"
  selfLink: /api/v1/namespaces/default/secrets/cuser
  uid: 87c9ef60-9c73-4fe3-b6ce-5637540fccb2
type: Opaque
```

    $ echo cm9vdA== | base64 --decode

```log
root
```

- create a secret named cprop with values password=toor and host=localhost :

```sh
  $ kubectl create secret generic cprop --from-literal=password=toor --from-literal=host=localhost

  $ kubectl describe secret cprop
```

```log
Name:         cprop
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  4 bytes
host:      9 bytes
```

- Create a busybox pod that loads and env variable called USERNAME from the variable username of the cmap secret then loads all cprop values as env variables in the same pod. The command to use in the container is sleep 1d :

You can check directly how to use a secret in a pod from the kubernetes documentation :

https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-environment-variables

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: secret-pod
spec:
  containers:
    - name: demo
      command: ["/bin/sh", "-c", "sleep 1d"]
      image: busybox
      env:
        # Define the environment variable
        - name: USERNAME
          valueFrom:
            secretKeyRef:
              name: cuser # The secret this value comes from.
              key: username # The key to fetch.
      envFrom:
        - secretRef:
            name: cprop
```

Now you can create the deployment using this command :

    $ kubectl create -f deploy.yaml

- display the environment variables of the container :

```sh
$ kubectl exec secret-pod -- env
```

```log
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=secret-pod
host=localhost
password=toor
USERNAME=root
HELLO_NODE_SERVICE_PORT=8080
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT=tcp://10.96.0.1:443
...
```

- Load the cprop secret as a volume named `foo` inside the nginx pod on the path '/var/foo'

https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-files-from-a-pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
    - name: demo
      image: nginx
      volumeMounts:
        - name: foo
          mountPath: "/var/foo"
  volumes:
    - name: foo
      secret:
        secretName: cprop
```

- Check the secrets from inside the pod :

```sh
$ kubectl exec nginx-pod -it -- sh
```

```log
# ls /var/foo
host  password
# cat /var/foo/host
localhost#
# cat /var/foo/password
toor#
```

- clean workspace

```sh
  $ kubectl delete pod nginx-pod secret-pod

  $ kubectl delete secret cprop cuser
```
