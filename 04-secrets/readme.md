# Deployment

In this section, we will focus on the ConfigMap objects.

## task-01

In this task we will create a configmap and configure it. We will show how to load it in a pod as an environment variable and as a volume.
