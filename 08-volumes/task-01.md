# Task 1

- create a persistence volume named foo-pv using the following configuration :

1. storage: 1Gi
2. access modes :
   1. ReadWriteOnce
   2. ReadWriteMany
3. mounted via a host path on '/etc/foo'

From the kubernetes documentation :

https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: foo-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
    - ReadWriteMany
  hostPath:
    path: "/etc/foo"
```

- Create a PersistentVolumeClaim called foo-pvc using this configuration :

1. request: 500Mi
2. accessMode: ReadWriteOnce

From the documentation :

https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolumeclaim

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: foo-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 500Mi
```

- check if the pvc was bounded :

```sh
$ k get pv
```

```log
NAME     CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM             STORAGECLASS   REASON   AGE
foo-pv   1Gi        RWO,RWX        Retain           Bound    default/foo-pvc   manual                  70s
```

```sh
$ k get pvc
```

```log
NAME      STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
foo-pvc   Bound    foo-pv   1Gi        RWO,RWX        manual         66s
```

- create a pod with two containers using this configuration :

1. pod name: foo

container 1:

1. name: busybox1
2. container arguments : while true; do echo Hello from busybox1; sleep 30; done
3. mount the persistence volume claim to /etc/foo

container 2:

1. name: busybox2
2. container arguments : while true; do echo Hello from busybox2; sleep 30; done
3. mount the persistence volume claim to /etc/foo

From the documentation :

https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-pod

```log
apiVersion: v1
kind: Pod
metadata:
  name: foo
spec:
  volumes:
    - name: task-pv-storage
      persistentVolumeClaim:
        claimName: foo-pvc
  containers:
    - name: busybox1
      image: busybox
      args: ["/bin/sh","-c","while true; do echo Hello from busybox1; sleep 30; done",]
      volumeMounts:
        - mountPath: "/etc/foo"
          name: task-pv-storage

    - name: busybox2
      image: busybox
      args: ["/bin/sh","-c","while true; do echo Hello from busybox2; sleep 30; done",]
      volumeMounts:
        - mountPath: "/etc/foo"
          name: task-pv-storage
```

- get the pod :

```sh
$ kubectl get pod
```

```log
NAME   READY   STATUS    RESTARTS   AGE
foo    2/2     Running   0          41s
```

- connect to the first container and create a file in /etc/foo/user that container the container user :

```sh
$ kubectl exec foo -c busybox1 -- /bin/sh -c 'echo $(whoami) > /etc/foo/user'
```

- connect to the second container and display the content of the file /etc/foo/user :

```sh
$ kubectl exec foo -c busybox2 -- /bin/sh -c 'cat /etc/foo/user'
```

```log
root
```
