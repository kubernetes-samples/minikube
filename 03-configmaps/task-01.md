# Task 1

- create a configmap named cuser with value username=root in the namespace prod (you have to create it) :

```sh
  $ kubectl create ns prod

  $ kubectl create cm cuser --from-literal=username=root -n prod

  $ kubectl describe cm cuser -n prod
```

```sh
Name:         cuser
Namespace:    prod
Labels:       <none>
Annotations:  <none>

Data
====
username:
----
root
Events:  <none>
```

- create a configmap named cprop with values password=toor and host=localhost :

As we will work on this namespace we can switch to it :

```sh
  $ kubectl config set-context $(kubectl config current-context) --namespace prod

  $ kubectl create cm cprop --from-literal=password=toor --from-literal=host=localhost

  $ kubectl describe cm cprop
```

```sh
Name:         cprop
Namespace:    prod
Labels:       <none>
Annotations:  <none>

Data
====
host:
----
localhost
password:
----
toor
Events:  <none>

```

- Create a busybox pod that loads and env variable called USERNAME from the variable username of the cmap configmap then loads all cprop values as env variables in the same pod. The command to use in the container is sleep 1d :

You can check directly how to use a configmap in a pod from the kubernetes documentation :

https://kubernetes.io/docs/concepts/configuration/configmap/

Check the second yaml example.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: configmap-demo-pod
  namespace: prod
spec:
  containers:
    - name: demo
      command: ["/bin/sh", "-c", "sleep 1d"]
      image: busybox
      env:
        # Define the environment variable
        - name: USERNAME # Notice that the case is different here
          # from the key name in the ConfigMap.
          valueFrom:
            configMapKeyRef:
              name: cuser # The ConfigMap this value comes from.
              key: username # The key to fetch.
      envFrom:
        - configMapRef:
            name: cprop
```

Now you can create the deployment using this command :

    $ kubectl create -f deploy.yaml

- display the environment variables of the container :

```sh
$ kubectl exec configmap-demo-pod -n prod -- env
```

```log
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=configmap-demo-pod
password=toor
USERNAME=root
host=localhost
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
KUBERNETES_SERVICE_HOST=10.96.0.1
KUBERNETES_SERVICE_PORT=443
HOME=/root
```

- Load the cprop as a volume inside an nginx pod on the path '/var/foo'

From the same documentation link of the last task.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-demo-pod
  namespace: prod
spec:
  containers:
    - name: demo
      image: nginx
      volumeMounts:
        - name: config
          mountPath: "/var/foo"
          readOnly: true
  volumes:
    # You set volumes at the Pod level, then mount them into containers inside that Pod
    - name: config
      configMap:
        # Provide the name of the ConfigMap you want to mount.
        name: cprop
```

- Check the configMap loaded by the pod

```sh
$ kubectl exec nginx-demo-pod -n prod -it -- sh
```

```log
# ls /var/foo
host  password
# cat /var/foo/host
localhost#
# cat /var/foo/password
toor#
```

- clean workspace

```sh
  $ kubectl delete pod nginx-demo-pod configmap-demo-pod

  $ kubectl config set-context $(kubectl config current-context) --namespace default

  $ kubectl delete ns prod
```
