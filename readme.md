# Minikube

In this project, I'll show you how to install kubernetes on your local machine using minikube and i'll provide some simple example to make it to understand the kubernetes ecosystem. So we'll talk about :

- Pods
- Deployments
- Services
- Network Policies
- Config Maps
- Secrets
- Storage
- ...

## Installation

### Kubectl

I've already installed kubectl which allows you to run commands against Kubernetes clusters. You can use kubectl to deploy applications, inspect and manage cluster resources, and view logs.

https://kubernetes.io/docs/tasks/tools/install-kubectl/

### Minikube

I'm using ubuntu 18.04 LTS. Minikube is a tool that runs a single-node Kubernetes cluster in a virtual machine on your personal computer.

https://kubernetes.io/fr/docs/setup/learning-environment/minikube/

https://kubernetes.io/docs/tasks/tools/install-minikube/

In my case, i want to use docker as a minikube driver :

    $ curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
        && chmod +x minikube
    $ sudo mkdir -p /usr/local/bin/
    $ sudo install minikube /usr/local/bin/

Minikube also supports a --driver=none option that runs the Kubernetes components on the host and not in a VM. Using this driver requires Docker and a Linux environment but not a hypervisor.

You have to run the last command as root. For non root users you use this command :

    $ minikube start --driver=docker

```log
😄  minikube v1.9.2 on Ubuntu 18.04
✨  Using the docker driver based on user configuration
👍  Starting control plane node m01 in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.18.0 preload ...
    > preloaded-images-k8s-v2-v1.18.0-docker-overlay2-amd64.tar.lz4: 542.91 MiB
🔥  Creating Kubernetes in docker container with (CPUs=2) (12 available), Memory=3900MB (15816MB available) ...
🐳  Preparing Kubernetes v1.18.0 on Docker 19.03.2 ...
    ▪ kubeadm.pod-network-cidr=10.244.0.0/16
🌟  Enabling addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
```

    $ kubectl version

```log
Client Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.3", GitCommit:"06ad960bfd03b39c8310aaf92d1e7c12ce618213", GitTreeState:"clean", BuildDate:"2020-02-12T13:43:46Z", GoVersion:"go1.13.7", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.0", GitCommit:"9e991415386e4cf155a24b1da15becaa390438d8", GitTreeState:"clean", BuildDate:"2020-03-25T14:50:46Z", GoVersion:"go1.13.8", Compiler:"gc", Platform:"linux/amd64"}
```

    $ docker container ls

| CONTAINER ID | IMAGE                              | COMMAND                | CREATED       | STATUS       | PORTS                                                                         | NAMES    |
| ------------ | ---------------------------------- | ---------------------- | ------------- | ------------ | ----------------------------------------------------------------------------- | -------- |
| 73a39cfb8079 | gcr.io/k8s-minikube/kicbase:v0.0.8 | "/usr/local/bin/entr…" | 8 minutes ago | Up 8 minutes | 127.0.0.1:32770->22/tcp, 127.0.0.1:32769->2376/tcp, 127.0.0.1:32768->8443/tcp | minikube |

## Some minikube tips

### services endpoints

Get the URL of the exposed Service to view the Service details:

    $ minikube service <service-name> --url

The service type must be a node port. Now you can access the service from your local machine.

The result contains the minikube ip and the node port. You can get the same ip using this command :

    $ minikube ip

### change context

If you change the minikube context (to use an another kubernetes cluster) and you would like to switch back to it later use this command :

    $ kubectl config use-context minikube

The minikube start command creates a kubectl context called “minikube”. This context contains the configuration to communicate with your Minikube cluster.

### dashboard

To access the minikube Kubernetes Dashboard, run this command in a shell after starting Minikube to get the address :

    $ minikube dashboard

### get access to the minikube docker environment

To work with the Docker daemon on your Mac/Linux host, run the last line from minikube docker-env :

    $ minikube docker-env

```log
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://172.17.0.4:2376"
export DOCKER_CERT_PATH="/home/salto/.minikube/certs"
export MINIKUBE_ACTIVE_DOCKERD="minikube"

# To point your shell to minikube's docker-daemon, run:
# eval $(minikube -p minikube docker-env)
```

This is the configuration, to activate it :

    $ eval $(minikube docker-env)

You can now use Docker at the command line of your host Mac/Linux machine to communicate with the Docker daemon inside the Minikube VM :

    $ docker image ls

```log
REPOSITORY                                TAG                 IMAGE ID            CREATED             SIZE
k8s.gcr.io/kube-proxy                     v1.18.0             43940c34f24f        12 days ago         117MB
k8s.gcr.io/kube-apiserver                 v1.18.0             74060cea7f70        12 days ago         173MB
k8s.gcr.io/kube-controller-manager        v1.18.0             d3e55153f52f        12 days ago         162MB
k8s.gcr.io/kube-scheduler                 v1.18.0             a31f78c7c8ce        12 days ago         95.3MB
kubernetesui/dashboard                    v2.0.0-rc6          cdc71b5a8a0e        3 weeks ago         221MB
k8s.gcr.io/pause                          3.2                 80d28bedfe5d        7 weeks ago         683kB
k8s.gcr.io/coredns                        1.6.7               67da37a9a360        2 months ago        43.8MB
kindest/kindnetd                          0.5.3               aa67fec7d7ef        5 months ago        78.5MB
k8s.gcr.io/etcd                           3.4.3-0             303ce5db0e90        5 months ago        288MB
kubernetesui/metrics-scraper              v1.0.2              3b08661dc379        5 months ago        40.1MB
gcr.io/k8s-minikube/storage-provisioner   v1.8.1              4689081edb10        2 years ago         80.8MB
```

Oops, this is not my machine. To switch back to your local docker host, run this command :

    $ eval $(minikube docker-env -u)

    $ docker image ls | head -n 5

```log
REPOSITORY                                                TAG                                        IMAGE ID            CREATED             SIZE
jenkins-docker-ci-cd                                      jenkins-pipeline-jenkins-docker-ci-cd-23   35e021cbba37        8 days ago          104MB
mouhamedali/jenkins-docker-ci-cd                          jenkins-pipeline-jenkins-docker-ci-cd-23   35e021cbba37        8 days ago          104MB
jenkins-docker-ci-cd                                      jenkins-pipeline-jenkins-docker-ci-cd-22   4960c9fbb9d1        8 days ago          104MB
mouhamedali/jenkins-docker-ci-cd                          jenkins-pipeline-jenkins-docker-ci-cd-22   4960c9fbb9d1        8 days ago          104MB
```

All the kubernetes config is in the ~/.kube/config file. you can see the content via this command :

    $ kubectl config get-contexts

### stop and delete the minikube cluster

To stop the local cluster :

    $ minikube stop

To delete the local cluster :

    $ minikube delete
