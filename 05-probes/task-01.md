# Task 1

- create a pod using the following configuration :

1. Pod name: probes
2. image : mouhamedali/static-site-nodejs:v1.0.0
3. container port : 8000
4. readiness probe :
   1. HTTP readinessProbe on path '/version' on port 8000
   2. starting after 1 minute
   3. internal between probes : 30 seconds
   4. number of tries before giving up is 5
5. liveness probe:
   1. run the command `ls` in the root directory
   2. starting after 2 minute
   3. internal between probes : 10 seconds
   4. number of tries before giving up is 5

You can the configuration of probes in the documentation :

liveness :

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-command

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-http-request

readiness :

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-readiness-probes

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: probes
  name: probes
spec:
  containers:
    - name: probes
      image: mouhamedali/static-site-nodejs:v1.0.0
      readinessProbe:
        httpGet:
          path: /version
          port: 8000
        initialDelaySeconds: 60
        periodSeconds: 30
        failureThreshold: 5
      livenessProbe:
        exec:
          command:
            - ls
            - /
        initialDelaySeconds: 120
        periodSeconds: 10
        failureThreshold: 5
```

- create the pod :

```sh
$ k create -f pod.yaml
```

- check the probes :

```sh
$ k describe pod probes
```

```log
State:          Running
      Started:      Fri, 15 May 2020 15:13:15 +0200
    Ready:          False
    Restart Count:  0
    Liveness:       exec [ls /] delay=120s timeout=1s period=10s #success=1 #failure=5
    Readiness:      http-get http://:8000/version delay=60s timeout=1s period=30s #success=1 #failure=5
    Environment:    <none>
```

```sh
$ kubectl get pods
```

```log
NAME     READY   STATUS    RESTARTS   AGE
probes   1/1     Running   0          2m9s
```

- clean workspace

```sh
  $ kubectl delete -f pod.yaml
```
